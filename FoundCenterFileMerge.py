#!/usr/bin/env python2.7

# Merge all Foundaiton Center csv files in directory, preserving header of first file and deleting all of the copyright and other extraneous stuff at bottom

import glob

files = glob.glob( '*.csv' )
outputFile="GrantsTotal.csv"

headerCount = 2
with open(outputFile, 'w' ) as result:
	for eachFile in files:
		opener = open(eachFile)
		lines = opener.readlines()
		for line in lines[headerCount:103]:
			if not ''.join(line).strip():
				print 'skip'
				break
    			result.write(line)
		opener.close()

		headerCount = 3
	
print "Complete"
